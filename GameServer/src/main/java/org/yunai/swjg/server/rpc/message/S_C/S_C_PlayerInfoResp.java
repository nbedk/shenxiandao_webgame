package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21204】: 角色信息
 */
public class S_C_PlayerInfoResp extends GameMessage {
    public static final short CODE = 21204;

    /**
     * 等级
     */
    private Short level;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 场景坐标X
     */
    private Short sceneX;
    /**
     * 场景坐标Y
     */
    private Short sceneY;

    public S_C_PlayerInfoResp() {
    }

    public S_C_PlayerInfoResp(Short level, String nickname, Short sceneX, Short sceneY) {
        this.level = level;
        this.nickname = nickname;
        this.sceneX = sceneX;
        this.sceneY = sceneY;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Short getSceneX() {
		return sceneX;
	}

	public void setSceneX(Short sceneX) {
		this.sceneX = sceneX;
	}
	public Short getSceneY() {
		return sceneY;
	}

	public void setSceneY(Short sceneY) {
		this.sceneY = sceneY;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_PlayerInfoResp struct = new S_C_PlayerInfoResp();
            struct.setLevel(byteArray.getShort());
            struct.setNickname(getString(byteArray));
            struct.setSceneX(byteArray.getShort());
            struct.setSceneY(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_PlayerInfoResp struct = (S_C_PlayerInfoResp) message;
            ByteArray byteArray = ByteArray.createNull(6);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            byteArray.putShort(struct.getLevel());
            putString(byteArray, nicknameBytes);
            byteArray.putShort(struct.getSceneX());
            byteArray.putShort(struct.getSceneY());
            return byteArray;
        }
    }
}