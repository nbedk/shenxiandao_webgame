package org.yunai.swjg.server.module.rep.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_RepEnterReq;

import javax.annotation.Resource;

/**
 * 玩家进入副本命令
 * User: yunai
 * Date: 13-5-13
 * Time: 下午4:19
 */
@Component
public class RepEnterCommand extends GameMessageCommand<C_S_RepEnterReq> {

    @Resource
    private RepSceneService repService;

    @Override
    public void execute(Online online, C_S_RepEnterReq msg) {
        repService.onPlayerSwitchRep(online, msg.getRep());
    }
}
