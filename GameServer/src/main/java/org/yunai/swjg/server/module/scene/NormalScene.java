package org.yunai.swjg.server.module.scene;

import org.slf4j.Logger;
import org.yunai.swjg.server.module.scene.core.AbstractScene;
import org.yunai.swjg.server.module.scene.core.template.SceneTemplate;
import org.yunai.yfserver.common.LoggerFactory;

/**
 * 场景
 * User: yunai
 * Date: 13-4-22
 * Time: 下午2:47
 */
public class NormalScene extends AbstractScene {

    private static final Logger LOGGER_SCENE = LoggerFactory.getLogger(LoggerFactory.Logger.scene, NormalScene.class);

    public NormalScene(SceneTemplate sceneTemplate) {
        super(sceneTemplate);
    }

    /**
     * 普通场景不分线，认为永远都是不满的
     *
     * @return false
     */
    @Override
    public boolean isFull() {
        return false;
    }
}
