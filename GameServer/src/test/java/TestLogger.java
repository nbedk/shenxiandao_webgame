import org.slf4j.Logger;
import org.yunai.swjg.server.rpc.message.C_S.C_S_LoginReq;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.message.IMessage;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-4-21
 * Time: 下午7:17
 */
public class TestLogger {

    public static void main1(String[] args) {
        Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.async, TestLogger.class);

        try {
            int y = 1 / 0;
        } catch (Exception e) {
//            LOGGER.error("[{}] entity[{}] dao[{}] error[{}]", CommonErrorLogInfo.DB_OPERATE_FAIL,
//                    JSON.toJSONString(new Object()), new Object(), ExceptionUtils.getStackTrace(e));
            LOGGER.error("fsfsfs[{}]", e);
        }
    }

    public static class Timer{
        private long start = 0l;

        public void reset(){
            start = System.currentTimeMillis();
        }

        public void printDrift(){
            System.out.println(System.currentTimeMillis() - start);
        }
    }

    public static void main(String[] args){
        Timer timer = new Timer();
        C_S_LoginReq loginReq = new C_S_LoginReq();
        int count = 10000000;

        timer.reset();
        for(int i = 0; i < count; i++){
            doSomeThing(true);
        }
        timer.printDrift();//62毫秒

        timer.reset();
        for(int i = 0; i < count; i++){
            doSomeThing(loginReq instanceof IMessage);
        }
        timer.printDrift();//94毫秒

        timer.reset();
        for (int i = 0; i < count; i++) {
            doSomeThing(loginReq.getCode() == C_S_LoginReq.CODE);
        }
        timer.printDrift();

//        timer.reset();
//        for(int i = 0; i < count; i++){
//            doSomeThing(man instanceof Man);
//        }
//        timer.printDrift();//78毫秒
//
//        timer.reset();
//        for(int i = 0; i < count; i++){
//            doSomeThing(son instanceof Man);
//        }
//        timer.printDrift();//94毫秒
//
//        timer.reset();
//        for(int i = 0; i < count; i++){
//            doSomeThing(man.getClass().isAssignableFrom(IPerson.class));
//        }
//        timer.printDrift();//4453毫秒
//
//        timer.reset();
//        for(int i = 0; i < count; i++){
//            doSomeThing(man.getClass().equals(Man.class));
//        }
//        timer.printDrift();//375毫秒
//
//        timer.reset();
//        for(int i = 0; i < count; i++){
//            doSomeThing(son.getClass().isInstance(man));
//        }
//        timer.printDrift();//4093毫秒
    }

    private static int doSomeThing(boolean bool){
        return new Random().nextInt(10);
    }
}
