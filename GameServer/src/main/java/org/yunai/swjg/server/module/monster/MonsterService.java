package org.yunai.swjg.server.module.monster;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.yunai.yfserver.common.LoggerFactory;

/**
 * 怪物逻辑
 * User: yunai
 * Date: 13-5-16
 * Time: 上午12:23
 */
@Service
public class MonsterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.monster, MonsterService.class);

//    public void killRepMonster(Online online, Integer index) {
//        AbstractScene scene = online.getScene();
//        if (scene == null) {
//            LOGGER.error("[killRepMonster] [online:{} kill monster in rep to failure] [online.scene not exists].", online.getPlayer().getId());
//            return;
//        }
//        // 设置状态
//        online.setGamingState(GamingState.IN_BATTLE);
//        // 判断怪物是否存在。若不存在，则发送怪物不存在消息
//        RepScene rep = (RepScene) scene;
//        RepTemplate.MonsterGroup monsterGroup = rep.getMonsterGroup(online, index);
//        if (monsterGroup == null) {
//            online.write(new S_C_RepAttackMonsterResp((byte) 0));
//            return;
//        }
//        // 发送怪物存在消息
//        online.write(new S_C_RepAttackMonsterResp((byte) 1));
//        // 开始战斗
//        Integer battleResult = new Random().nextInt(2); // TODO 目前先随即下
//        if (battleResult == 0) { // 战斗输了
//            online.write(new S_C_RepAttackMonsterBattleResp(battleResult.byteValue(), (byte) 0));
//            return;
//        }
//        // 战斗成功(标记怪被杀了)
//        rep.onMonsterKilled(online, index); // TODO 触发怪物死掉事件
//        boolean isMonsterAllKilled = rep.isMonsterAllKilled(online);
//
//        //
//        // 1. 经验 2. 体力 3. 道具 4. 金钱奖励 5. 阅历奖励 6. 战斗评分
//        // 奖励经验
//
//        online.write(new S_C_RepAttackMonsterBattleResp(battleResult.byteValue(), isMonsterAllKilled ? (byte) 1 : (byte) 0));
//    }
}
