package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20607】: 场景中玩家移动请求
 */
public class C_S_SceneMoveReq extends GameMessage {
    public static final short CODE = 20607;

    /**
     * 当前地址坐标x
     */
    private Short fromX;
    /**
     * 当前地址坐标y
     */
    private Short fromY;
    /**
     * 目标场景坐标x
     */
    private Short toSceneX;
    /**
     * 目标场景坐标y
     */
    private Short toSceneY;

    public C_S_SceneMoveReq() {
    }

    public C_S_SceneMoveReq(Short fromX, Short fromY, Short toSceneX, Short toSceneY) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toSceneX = toSceneX;
        this.toSceneY = toSceneY;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Short getFromX() {
		return fromX;
	}

	public void setFromX(Short fromX) {
		this.fromX = fromX;
	}
	public Short getFromY() {
		return fromY;
	}

	public void setFromY(Short fromY) {
		this.fromY = fromY;
	}
	public Short getToSceneX() {
		return toSceneX;
	}

	public void setToSceneX(Short toSceneX) {
		this.toSceneX = toSceneX;
	}
	public Short getToSceneY() {
		return toSceneY;
	}

	public void setToSceneY(Short toSceneY) {
		this.toSceneY = toSceneY;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_SceneMoveReq struct = new C_S_SceneMoveReq();
            struct.setFromX(byteArray.getShort());
            struct.setFromY(byteArray.getShort());
            struct.setToSceneX(byteArray.getShort());
            struct.setToSceneY(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_SceneMoveReq struct = (C_S_SceneMoveReq) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putShort(struct.getFromX());
            byteArray.putShort(struct.getFromY());
            byteArray.putShort(struct.getToSceneX());
            byteArray.putShort(struct.getToSceneY());
            return byteArray;
        }
    }
}