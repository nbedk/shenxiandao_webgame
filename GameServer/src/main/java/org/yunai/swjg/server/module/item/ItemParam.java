package org.yunai.swjg.server.module.item;

/**
 * 其他系统发给背包模块做一些逻辑
 * User: yunai
 * Date: 13-5-11
 * Time: 上午8:20
 */
public class ItemParam {
    /**
     * 道具编号
     */
    private Integer templateId;
    /**
     * 道具属俩个
     */
    private Integer count;

    public ItemParam(Integer templateId, Integer count) {
        this.templateId = templateId;
        this.count = count;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}