package org.yunai.swjg.server.module.activity;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.constants.SceneConstants;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.msg.ScheduleCronActivityChangeStatusMessage;
import org.yunai.swjg.server.module.activity.template.ActivityTemplate;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.module.scene.NormalSceneService;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.schedule.ScheduleService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 活动管理
 * User: yunai
 * Date: 13-5-20
 * Time: 上午11:30
 */
@Service
public class ActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.activity, ActivityService.class);

    @Resource
    private NormalSceneService normalSceneService;
    @Resource
    private RepSceneService repSceneService;
    @Resource
    private ScheduleService scheduleService;

    /**
     * 活动集合<br />
     * KEY: 活动编号
     * VALUE: 活动对象
     */
    private Map<Integer, AbstractActivity> activities = new LinkedHashMap<>();
    /**
     * 赤炎兽BOSS活动
     */
    private BossActivity bossActivityA;

    @PostConstruct
    public void init() {
        AbstractActivity activity;
        for (ActivityTemplate template : ActivityTemplate.getList()) {
            switch (template.getId()) {
                case 1:
                    activity = bossActivityA = new BossActivity(1, SceneConstants.SCENE_BOSS_ACTIVITY_A);
                    break;
                default:
                    throw new Error("出现未知活动编号，赶紧检查！");
            }
            activities.put(activity.getId(), activity);
            scheduleService.schedule(new ScheduleCronActivityChangeStatusMessage(activity, ActivityDef.Status.PREPARE, template.getPrepareCronTime()));
            scheduleService.schedule(new ScheduleCronActivityChangeStatusMessage(activity, ActivityDef.Status.PROCESS, template.getProcessCronTime()));
            scheduleService.schedule(new ScheduleCronActivityChangeStatusMessage(activity, ActivityDef.Status.END, template.getEndCronTime()));
        }
    }

    public void list(Online online) {

    }

    public void join(Online online, Integer actId) {
        AbstractActivity activity = activities.get(actId);
        if (activity == null) {
            LOGGER.error("[join] [activity({}) is not found].", activity);
            return;
        }
        activity.join(online);
    }

    public BossActivity getBossActivityA() {
        return bossActivityA;
    }

    /**
     * 相同时间里，有且只有一个正在进行中的BOSS活动副本
     *
     * @return 进行中的BOSS副本
     */
    public BossActivity getBossActivity() {
        if (bossActivityA.getStatus() == ActivityDef.Status.PROCESS) {
            return bossActivityA;
        }
        return null;
    }

    /**
     * 修改活动状态
     *
     * @param actId  活动编号
     * @param status 目标活动状态
     */
    public void changeStatus(Integer actId, ActivityDef.Status status) {
        AbstractActivity activity = activities.get(actId);
        if (activity == null) {
            LOGGER.error("[changeStatus] [activity({}) is not found].", activity);
            return;
        }
        switch (status) {
            case PREPARE:
                prepare(activity.getId());
                break;
            case PROCESS:
                process(activity.getId());
                break;
            case END:
                end(activity.getId());
                break;
        }
    }

    public void prepare(Integer actId) {
        AbstractActivity activity = activities.get(actId);
        if (activity == null) {
            LOGGER.error("[prepare] [activity({}) is not found].", activity);
            return;
        }
        activity.prepare();
    }

    public void process(Integer actId) {
        AbstractActivity activity = activities.get(actId);
        if (activity == null) {
            LOGGER.error("[process] [activity({}) is not found].", activity);
            return;
        }
        activity.process();
    }

    public void end(Integer actId) {
        AbstractActivity activity = activities.get(actId);
        if (activity == null) {
            LOGGER.error("[end] [activity({}) is not found].", activity);
            return;
        }
        activity.end();
    }
}
