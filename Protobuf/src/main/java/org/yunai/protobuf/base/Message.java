package org.yunai.protobuf.base;

/**
 * 消息接口
 * User: yunai
 * Date: 13-3-14
 * Time: 下午2:37
 */
public interface Message extends Struct {
    short getCode();
}
