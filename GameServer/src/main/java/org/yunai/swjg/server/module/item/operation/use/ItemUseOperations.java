package org.yunai.swjg.server.module.item.operation.use;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 道具使用操作集合
 * User: yunai
 * Date: 13-6-2
 */
@Component
public class ItemUseOperations {

    private final List<ItemUseOperation> operations;

    @Resource
    private UseEquipmentOperation useEquipmentOperation;
    @Resource
    private UseEquipmentReelOperation useEquipmentReelOperation;
    @Resource
    private UseMedicineReelOperation useMedicineReelOperation;
    @Resource
    private UseMedicineConsumableOperation useMedicineConsumableOperation;

    public ItemUseOperations() {
        this.operations = new ArrayList<>();
    }

    @PostConstruct
    public void init() {
        operations.add(useEquipmentOperation);
        operations.add(useEquipmentReelOperation);
        operations.add(useMedicineReelOperation);
        operations.add(useMedicineConsumableOperation);
    }

    /**
     * 获得对应的使用操作
     *
     * @param player   玩家信息
     * @param item     道具
     * @param targetId 目标编号
     * @return 使用操作
     */
    public ItemUseOperation getSuitableOperation(Player player, Item item, int targetId) {
        for (ItemUseOperation operation : operations) {
            if (operation.isSuitable(player, item, targetId)) {
                return operation;
            }
        }
        return null;
    }
}