package org.yunai.yfserver.common.constants;

/**
 * 定义公用错误内容
 * User: yunai
 * Date: 13-4-21
 * Time: 下午7:44
 */
public class CommonErrorLogInfo {

    /**
     * 数据库操作失败
     */
    public static final String DB_OPERATE_FAIL = "DB.ERR.OPR";

    /**
     * 无效的逻辑状态
     */
    public static final String INVALID_STATE = "STATE.ERR.INVALID";
}