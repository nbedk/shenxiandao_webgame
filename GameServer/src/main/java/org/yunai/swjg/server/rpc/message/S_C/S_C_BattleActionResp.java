package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20002】: 战斗战报响应
 */
public class S_C_BattleActionResp extends GameMessage {
    public static final short CODE = 20002;

    /**
     * 战斗结果
     */
    private Byte win;

    public S_C_BattleActionResp() {
    }

    public S_C_BattleActionResp(Byte win) {
        this.win = win;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getWin() {
		return win;
	}

	public void setWin(Byte win) {
		this.win = win;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_BattleActionResp struct = new S_C_BattleActionResp();
            struct.setWin(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_BattleActionResp struct = (S_C_BattleActionResp) message;
            ByteArray byteArray = ByteArray.createNull(1);
            byteArray.create();
            byteArray.putByte(struct.getWin());
            return byteArray;
        }
    }
}