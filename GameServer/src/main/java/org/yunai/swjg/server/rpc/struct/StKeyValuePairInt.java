package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 键值对结构体(值类型为Int
 */
public class StKeyValuePairInt implements IStruct {
    /**
     * 键
     */
    private Integer key;
    /**
     * 值
     */
    private Integer val;

    public StKeyValuePairInt() {
    }

    public StKeyValuePairInt(Integer key, Integer val) {
        this.key = key;
        this.val = val;
    }

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}
	public Integer getVal() {
		return val;
	}

	public void setVal(Integer val) {
		this.val = val;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StKeyValuePairInt struct = new StKeyValuePairInt();
            struct.setKey(byteArray.getInt());
            struct.setVal(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StKeyValuePairInt struct = (StKeyValuePairInt) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getKey());
            byteArray.putInt(struct.getVal());
            return byteArray;
        }
    }
}