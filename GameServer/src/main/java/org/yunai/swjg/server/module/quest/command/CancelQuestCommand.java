package org.yunai.swjg.server.module.quest.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.quest.QuestService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_CancelQuestReq;

import javax.annotation.Resource;

/**
 * 取消任务消息命令
 * User: yunai
 * Date: 13-5-10
 * Time: 下午3:44
 */
@Controller
public class CancelQuestCommand extends GameMessageCommand<C_S_CancelQuestReq> {

    @Resource
    private QuestService questService;

    @Override
    public void execute(Online online, C_S_CancelQuestReq msg) {
        questService.cancel(online, msg.getQuestId());
    }
}
