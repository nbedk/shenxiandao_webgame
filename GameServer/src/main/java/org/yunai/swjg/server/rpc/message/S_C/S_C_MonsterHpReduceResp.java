package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20203】: 怪物HP下降响应
 */
public class S_C_MonsterHpReduceResp extends GameMessage {
    public static final short CODE = 20203;

    /**
     * 当前血量
     */
    private Integer hpCur;
    /**
     * 总血量
     */
    private Integer hpMax;
    /**
     * 怪物编号，唯一标识
     */
    private Integer id;
    /**
     * 下降血量
     */
    private Integer lossHp;

    public S_C_MonsterHpReduceResp() {
    }

    public S_C_MonsterHpReduceResp(Integer hpCur, Integer hpMax, Integer id, Integer lossHp) {
        this.hpCur = hpCur;
        this.hpMax = hpMax;
        this.id = id;
        this.lossHp = lossHp;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getHpCur() {
		return hpCur;
	}

	public void setHpCur(Integer hpCur) {
		this.hpCur = hpCur;
	}
	public Integer getHpMax() {
		return hpMax;
	}

	public void setHpMax(Integer hpMax) {
		this.hpMax = hpMax;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getLossHp() {
		return lossHp;
	}

	public void setLossHp(Integer lossHp) {
		this.lossHp = lossHp;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_MonsterHpReduceResp struct = new S_C_MonsterHpReduceResp();
            struct.setHpCur(byteArray.getInt());
            struct.setHpMax(byteArray.getInt());
            struct.setId(byteArray.getInt());
            struct.setLossHp(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_MonsterHpReduceResp struct = (S_C_MonsterHpReduceResp) message;
            ByteArray byteArray = ByteArray.createNull(16);
            byteArray.create();
            byteArray.putInt(struct.getHpCur());
            byteArray.putInt(struct.getHpMax());
            byteArray.putInt(struct.getId());
            byteArray.putInt(struct.getLossHp());
            return byteArray;
        }
    }
}