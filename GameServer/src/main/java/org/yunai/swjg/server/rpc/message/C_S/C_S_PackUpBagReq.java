package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20805】: 背包整理请求
 */
public class C_S_PackUpBagReq extends GameMessage {
    public static final short CODE = 20805;

    /**
     * 背包编号
     */
    private Byte bagId;

    public C_S_PackUpBagReq() {
    }

    public C_S_PackUpBagReq(Byte bagId) {
        this.bagId = bagId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getBagId() {
		return bagId;
	}

	public void setBagId(Byte bagId) {
		this.bagId = bagId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_PackUpBagReq struct = new C_S_PackUpBagReq();
            struct.setBagId(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_PackUpBagReq struct = (C_S_PackUpBagReq) message;
            ByteArray byteArray = ByteArray.createNull(1);
            byteArray.create();
            byteArray.putByte(struct.getBagId());
            return byteArray;
        }
    }
}