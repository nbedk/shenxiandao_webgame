package org.yunai.yfserver.util;

import java.util.Random;

/**
 * 数学工具类
 * User: yunai
 * Date: 13-5-17
 * Time: 下午10:28
 */
public class MathUtils {

    private static final Random random = new Random();

    private MathUtils() {
    }

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

    public static int nextInt(int n) {
        return random.nextInt(n);
    }

    /**
     * 除法。大值/小值
     *
     * @param a 值A
     * @param b 值B
     * @return 结果
     */
    public static long div(long a, long b) {
        return a > b ? a / b : b / a;
    }

    public static int min(int... array) {
        if (array.length == 0) {
            throw new IllegalArgumentException("array的长度不能为空！");
        }
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            min = Math.min(min, array[i]);
        }
        return min;
    }

    public static int ceil2Int(Double val) {
        return (int) Math.ceil(val);
    }

    public static long ceil2Long(double val) {
        return (long) Math.ceil(val);
    }

    public static int floor2Int(Double val) {
        return (int) Math.floor(val);
    }

    public static long floor2Long(double val) {
        return (long) Math.floor(val);
    }

    /**
     * 计算所需位置<br />
     * 即result = val % divideVal != 0 ? val / divideVal + 1 : val / divideVal;
     *
     * @param val       数量。当数量小于等于0时，返回0
     * @param divideVal 每个位置最大能装数量
     * @return 计算所需位置
     */
    public static long ceilX(long val, long divideVal) {
        return val <= 0 ? 0 : (val - 1 + divideVal) / divideVal;
    }

    /**
     * 计算所需位置<br />
     * 即result = val % divideVal != 0 ? val / divideVal + 1 : val / divideVal;
     *
     * @param val       数量。当数量小于等于0时，返回0
     * @param divideVal 每个位置最大能装数量
     * @return 计算所需位置
     */
    public static int ceilX(int val, int divideVal) {
        return val <= 0 ? 0 : (val - 1 + divideVal) / divideVal;
    }
}
