package org.yunai.yfserver.async;

/**
 * IO操作接口<br />
 * <b>保证doStart/doIo/doFinish的实现都是原子性的。即，当doStart执行了，但doIo不执行也没关系。</b><br />
 * 如果真的无法保证原子性，那记得注释下，想办法达到。
 *
 * User: yunai
 * Date: 13-3-28
 * Time: 上午10:09
 */
public interface IIoOperation {

    /**
     * 操作状态<br />
     * INITIALIZED => STARTED => IO_DONE => FINISHED
     */
    public static enum State {
        /** 初始化结束 */
        INITIALIZED,
        /** 启动结束 */
        STARTED,
        /** 执行结束 */
        IO_DONE,
        /** 停止结束 */
       FINISHED
    }

    /**
     * 启动执行IO前的操作
     * @return 执行后的状态
     */
    State doStart();

    /**
     * 启动IO操作
     * @return 执行后的操作
     */
    State doIo();

    /**
     * 结束IO操作
     * @return 执行后的状态
     */
    State doFinish();

}
