package org.yunai.swjg.server.module.battle.operation;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.battle.BattleDef;
import org.yunai.swjg.server.module.battle.unit.BattleTeam;
import org.yunai.swjg.server.module.monster.vo.Monster;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;
import org.yunai.swjg.server.rpc.message.S_C.S_C_BattleActionResp;

/**
 * 玩家与怪物战斗操作基类.
 * User: yunai
 * Date: 13-5-17
 * Time: 下午9:49
 */
public abstract class PVMBattleOperation extends BaseBattleOperation {

    protected Online online;
    protected VisibleMonster monster;

    public PVMBattleOperation(Online online, VisibleMonster monster, boolean attackIsFullHp, boolean defenseIsFullHp) {
        // 构建进攻队伍
        super(online, attackIsFullHp);
        // 变量赋值
        this.online = online;
        this.monster = monster;
        // 构建怪物防守队伍
        super.defenseTeam = new BattleTeam(BattleDef.Team.DEFENSE);
        for (Monster monsterUnit : monster.getMonsters()) {
            super.addBattleUnit(monsterUnit, super.defenseTeam, 1, defenseIsFullHp);
        }
    }

    @Override
    protected void endImpl(boolean attWin) {
        // TODO 奖励

        // 战斗消息
        S_C_BattleActionResp battleActionResp = new S_C_BattleActionResp(attWin ? (byte) 1 : (byte) 0);

        // 调用子类实现
        pvmEndImpl(attWin);
    }

    protected abstract void pvmEndImpl(boolean attWin);
}
