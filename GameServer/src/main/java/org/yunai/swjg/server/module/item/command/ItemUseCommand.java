package org.yunai.swjg.server.module.item.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.ItemService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_ItemUseReq;

import javax.annotation.Resource;

/**
 * 道具使用命令<br />
 * User: yunai
 * Date: 13-6-2
 * Time: 上午10:03
 */
@Controller
public class ItemUseCommand extends GameMessageCommand<C_S_ItemUseReq> {

    @Resource
    private ItemService itemService;

    @Override
    protected void execute(Online online, C_S_ItemUseReq msg) {
        itemService.useItem(online, msg.getBagId(), msg.getBagIndex().intValue(),
                msg.getWearId(), msg.getTargetId(), msg.getParam());
    }
}