package org.yunai.swjg.server.module.quest.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.currency.Currency;
import org.yunai.swjg.server.module.item.ItemParam;
import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.swjg.server.module.quest.condition.IQuestCondition;
import org.yunai.swjg.server.module.quest.condition.QuestConditionFactory;
import org.yunai.yfserver.util.CsvUtil;
import org.yunai.yfserver.util.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * 任务模版
 * User: yunai
 * Date: 13-5-8
 * Time: 下午11:21
 */
public class QuestTemplate {
    /**
     * 编号
     */
    private Integer id;
    /**
     * 任务名
     */
    private String title;
    /**
     * 任务类型
     */
    private QuestDef.Type type;
    /**
     * 接受任务最小等级
     */
    private Short acceptMinLevel;
    /**
     * 任务内容
     */
    private String content;
    /**
     * 任务所在区域
     */
    private Integer region;
    /**
     * 接受任务NPC
     */
    private Integer acceptNPC;
    /**
     * 完成任务NPC
     */
    private Integer finishNPC;
    /**
     * 前置任务字符串,用"|"分隔
     */
    private String preQuests;
    /**
     * 任务完成条件
     */
    private List<IQuestCondition> finishConditions;
    /**
     * 奖励金钱类型
     */
    private Currency bonusCurrency;
    /**
     * 奖励金币数量
     */
    private Integer bonusCurrencyNum;
    /**
     * 奖励经验值
     */
    private Integer bonusExp;
    /**
     * 奖励道具列表
     */
    private List<ItemParam> bonusItems;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public QuestDef.Type getType() {
        return type;
    }

    public Short getAcceptMinLevel() {
        return acceptMinLevel;
    }

    public String getContent() {
        return content;
    }

    public Integer getRegion() {
        return region;
    }

    public Integer getAcceptNPC() {
        return acceptNPC;
    }

    public Integer getFinishNPC() {
        return finishNPC;
    }

    public String getPreQuests() {
        return preQuests;
    }

    public List<IQuestCondition> getFinishConditions() {
        return finishConditions;
    }

    public Currency getBonusCurrency() {
        return bonusCurrency;
    }

    public Integer getBonusCurrencyNum() {
        return bonusCurrencyNum;
    }

    public Integer getBonusExp() {
        return bonusExp;
    }

    public List<ItemParam> getBonusItems() {
        return bonusItems;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, QuestTemplate> templates = new HashMap<>();

    public static Map<Integer, QuestTemplate> getAll() {
        return Collections.unmodifiableMap(templates);
    }

    public static void load() {
        CsvReader reader = null;
        // 任务模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/quest/quest_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                QuestTemplate template = genQuestTemplate(reader);
                templates.put(template.id, template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static QuestTemplate genQuestTemplate(CsvReader reader) throws IOException {
        QuestTemplate template = new QuestTemplate();
        template.id = CsvUtil.getInt(reader, "id", -1);
        template.title = CsvUtil.getString(reader, "title", "");
        template.type = QuestDef.Type.valueOf(CsvUtil.getInt(reader, "type", 0));
        template.acceptMinLevel = CsvUtil.getShort(reader, "acceptMinLevel", (short) -1);
        template.content = CsvUtil.getString(reader, "content", "");
        template.region = CsvUtil.getInt(reader, "region", 0);
        template.acceptNPC = CsvUtil.getInt(reader, "acceptNPC", 0);
        template.finishNPC = CsvUtil.getInt(reader, "finishNPC", 0);
        template.preQuests = CsvUtil.getString(reader, "preQuests", "");
        // 完成任务条件
        String[] finishConditions = StringUtils.split(CsvUtil.getString(reader, "finishConditions", ""), ";");
        List<IQuestCondition> finishConditionList = finishConditions.length > 0 ? new ArrayList<>(finishConditions.length) : Collections.EMPTY_LIST;
        for (String finishCondition : finishConditions) {
            finishConditionList.add(QuestConditionFactory.create(finishCondition));
        }
        template.finishConditions = finishConditionList;
        template.bonusCurrency = Currency.valueOf(CsvUtil.getInt(reader, "bonusCurrency", 0));
        template.bonusCurrencyNum = CsvUtil.getInt(reader, "bonusCurrencyNum", 0);
        template.bonusExp = CsvUtil.getInt(reader, "bonusExp", 0);
        // 完成任务道具奖励
        String[] bonusItems = StringUtils.split(CsvUtil.getString(reader, "bonusItems", ""), ";");
        List<ItemParam> bonusItemList = bonusItems.length > 0 ? new ArrayList<>(bonusItems.length) : Collections.EMPTY_LIST;
        for (String bonusItem : bonusItems) {
            int[] bonusItemParam = StringUtils.splitInt(bonusItem, "\\|");
            bonusItemList.add(new ItemParam(bonusItemParam[0], bonusItemParam[1]));
        }
        template.bonusItems = bonusItemList;
        return template;
    }
}
