package org.yunai.swjg.server.core.thread;

import org.yunai.yfserver.async.IIoSerialOperation;

/**
 * 对不在线的其他玩家进行操作
 * User: yunai
 * Date: 13-5-22
 * Time: 上午12:31
 */
public class OfflineOtherPlayerOperation implements IIoSerialOperation {

    private final AbstractOtherPlayerOperation operation;

    public OfflineOtherPlayerOperation(AbstractOtherPlayerOperation operation) {
        this.operation = operation;
    }

    @Override
    public Integer getSerialKey() {
        return operation.getPlayerId();
    }

    @Override
    public State doStart() {
        return State.STARTED;
    }

    @Override
    public State doIo() {
        operation.doOffline();
        return State.FINISHED;
    }

    @Override
    public State doFinish() {
        throw new IllegalStateException("该方法不会调用到。");
    }

}
