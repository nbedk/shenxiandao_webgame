package org.yunai.swjg.server.module.rep.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_RepLeaveReq;

import javax.annotation.Resource;

/**
 * 离开副本命令
 * User: yunai
 * Date: 13-5-14
 * Time: 下午8:48
 */
@Controller
public class RepLeaveCommand extends GameMessageCommand<C_S_RepLeaveReq> {

    @Resource
    private RepSceneService repSceneService;

    @Override
    protected void execute(Online online, C_S_RepLeaveReq msg) {
        repSceneService.onPlayerSwitchRepToNormalScene(online);
    }
}
