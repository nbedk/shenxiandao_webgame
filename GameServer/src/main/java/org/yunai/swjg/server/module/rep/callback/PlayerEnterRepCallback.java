package org.yunai.swjg.server.module.rep.callback;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.annotation.MainThread;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.player.PlayerExitReason;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家进入副本回调
 * User: yunai
 * Date: 13-5-13
 * Time: 下午4:25
 */
public class PlayerEnterRepCallback implements SceneCallable {

    public static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.player, PlayerEnterRepCallback.class);

    private static RepSceneService repService;
    private static OnlineContextService onlineContextService;

    static {
        repService = BeanManager.getBean(RepSceneService.class);
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
    }

    private final Integer playerId;
    private final Integer repId;

    public PlayerEnterRepCallback(Integer playerId, Integer repId) {
        this.playerId = playerId;
        this.repId = repId;
    }

    @Override
    @MainThread
    public void call() {
        Online online = onlineContextService.getPlayer(this.playerId);
        if (online == null) {
            LOGGER.error("[call] [online:{} is not found].", this.playerId);
            return;
        }
        boolean success = repService.onPlayerEnterRep(online, repId);
        if (!success) { // 进入场景失败则断开帐号
            online.setExitReason(PlayerExitReason.SERVER_ERROR);
            online.disconnect();
        }
    }
}
