package org.yunai.swjg.server.module.scene.core.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.scene.core.SceneDef;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 场景模版
 * User: yunai
 * Date: 13-5-19
 * Time: 上午10:31
 */
public class SceneTemplate {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 名字
     */
    private String name;
    /**
     * 场景类型
     */
    private SceneDef.Type type;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public SceneDef.Type getType() {
        return type;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, SceneTemplate> templates;

    public static SceneTemplate get(Integer id) {
        return templates.get(id);
    }

    public static void load() {
        CsvReader reader = null;
        // 副本模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/scene/scene_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                SceneTemplate template = read(reader);
                templates.put(template.getId(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static SceneTemplate read(CsvReader reader) throws IOException {
        SceneTemplate template = new SceneTemplate();
        template.id = CsvUtil.getInt(reader, "id", 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.type = SceneDef.Type.valueOf(CsvUtil.getShort(reader, "type", (short) 0));
        return template;
    }
}
