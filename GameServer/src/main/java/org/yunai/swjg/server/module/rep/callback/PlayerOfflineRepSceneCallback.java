package org.yunai.swjg.server.module.rep.callback;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.annotation.MainThread;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.player.PlayerExitReason;
import org.yunai.swjg.server.module.player.PlayerService;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家下线的场景回调
 * User: yunai
 * Date: 13-5-14
 * Time: 下午9:40
 */
public class PlayerOfflineRepSceneCallback implements SceneCallable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.rep, PlayerOfflineRepSceneCallback.class);

    private static PlayerService playerService;
    private static OnlineContextService onlineContextService;
    private static RepSceneService repSceneService;
    static {
        playerService = BeanManager.getBean(PlayerService.class);
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
        repSceneService = BeanManager.getBean(RepSceneService.class);
    }

    private final Integer playerId;

    public PlayerOfflineRepSceneCallback(Integer playerId) {
        this.playerId = playerId;
    }

    /**
     * 重新调用Online下线<br />
     * 另外，将玩家所在场景设置成进入副本前
     */
    @Override
    @MainThread
    public void call() {
        Online online = onlineContextService.getPlayer(this.playerId);
        if (online == null) {
            LOGGER.error("[call] [online:{} is not found].", this.playerId);
            return;
        }
        Player player = online.getPlayer();
        if (player.isScenePreInfoNull()) { // 应该是不会有这个情况的，log下以防万一
            LOGGER.error("[call] [player({}) scenePreInfo is null] [this error is important].", playerId);
            online.setExitReason(PlayerExitReason.SERVER_ERROR);
            online.disconnect();
            return;
        }
        // 回到进入副本前的场景
        repSceneService.gotoBeforeScene(online);
        // 继续下线流程
        playerService.onPlayerLogout(online);
    }
}