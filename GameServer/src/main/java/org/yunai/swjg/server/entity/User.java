package org.yunai.swjg.server.entity;

/**
 * 用户帐号
 * User: yunai
 * Date: 13-3-28
 * Time: 下午7:52
 */
public class User {

    /** 帐号编号 */
    private Integer id;
    /** 用户名 */
    private String userName;
    /** 密码 */
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
