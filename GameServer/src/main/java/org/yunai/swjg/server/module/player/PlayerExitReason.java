package org.yunai.swjg.server.module.player;

/**
 * 玩家退出服务器原因枚举
 * User: yunai
 * Date: 13-4-2
 * Time: 下午12:47
 */
public enum PlayerExitReason {
    /** 玩家点击退出或直接断开连接 */
    LOGOUT,
    /** GS停机 */
    SERVER_SHUTDOWN,
    /** 服务器处理出错 */
    SERVER_ERROR,
    /** TODO 服务器人数已满 */
    SERVER_IS_FULL,
    /** TODO GM踢掉 */
    GM_KICK,
    /** 自己把自己踢掉 */
    MULTI_LOGIN,
    /** TODO 防沉迷阻止登录或者踢出 */
    WALLOW_KICK,
    /** TODO GS踢人*/
    SERVER_KICK,
}
