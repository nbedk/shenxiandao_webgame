package org.yunai.swjg.server.core.message;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.core.session.GameSession;
import org.yunai.yfserver.message.sys.SessionOpenedMessage;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 游戏Session会话打开消息<br />
 * 1.用于创建Online
 * User: yunai
 * Date: 13-3-27
 * Time: 下午2:35
 */
public class GameSessionOpenedMessage extends SessionOpenedMessage<GameSession> {

    private static OnlineContextService onlineContextService;

    static {
        GameSessionOpenedMessage.onlineContextService = BeanManager.getBean(OnlineContextService.class);
    }

    private final GameSession session;

    public GameSessionOpenedMessage(GameSession session) {
        this.session = session;
    }

    @Override
    public void execute() {
        onlineContextService.addOnline(new Online(session));
    }
}
