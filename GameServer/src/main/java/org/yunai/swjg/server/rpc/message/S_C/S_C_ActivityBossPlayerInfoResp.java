package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21615】: BOSS活动玩家信息
 */
public class S_C_ActivityBossPlayerInfoResp extends GameMessage {
    public static final short CODE = 21615;

    /**
     * 总伤害
     */
    private Integer allDamage;
    /**
     * 剩余复活时间，秒
     */
    private Integer aliveTime;
    /**
     * 是否已经死亡
     */
    private Byte dead;
    /**
     * 鼓舞次数
     */
    private Byte encourageCount;

    public S_C_ActivityBossPlayerInfoResp() {
    }

    public S_C_ActivityBossPlayerInfoResp(Integer allDamage, Integer aliveTime, Byte dead, Byte encourageCount) {
        this.allDamage = allDamage;
        this.aliveTime = aliveTime;
        this.dead = dead;
        this.encourageCount = encourageCount;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getAllDamage() {
		return allDamage;
	}

	public void setAllDamage(Integer allDamage) {
		this.allDamage = allDamage;
	}
	public Integer getAliveTime() {
		return aliveTime;
	}

	public void setAliveTime(Integer aliveTime) {
		this.aliveTime = aliveTime;
	}
	public Byte getDead() {
		return dead;
	}

	public void setDead(Byte dead) {
		this.dead = dead;
	}
	public Byte getEncourageCount() {
		return encourageCount;
	}

	public void setEncourageCount(Byte encourageCount) {
		this.encourageCount = encourageCount;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ActivityBossPlayerInfoResp struct = new S_C_ActivityBossPlayerInfoResp();
            struct.setAllDamage(byteArray.getInt());
            struct.setAliveTime(byteArray.getInt());
            struct.setDead(byteArray.getByte());
            struct.setEncourageCount(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ActivityBossPlayerInfoResp struct = (S_C_ActivityBossPlayerInfoResp) message;
            ByteArray byteArray = ByteArray.createNull(10);
            byteArray.create();
            byteArray.putInt(struct.getAllDamage());
            byteArray.putInt(struct.getAliveTime());
            byteArray.putByte(struct.getDead());
            byteArray.putByte(struct.getEncourageCount());
            return byteArray;
        }
    }
}