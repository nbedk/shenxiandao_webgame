package org.yunai.swjg.server.module.user.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.user.UserService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_LoginReq;

import javax.annotation.Resource;

/**
 * 用户登录命令
 * User: yunai
 * Date: 13-3-28
 * Time: 下午8:16
 */
@Controller
public class UserLoginCommand extends GameMessageCommand<C_S_LoginReq> {

    @Resource
    private UserService userService;

    @Override
    public void execute(Online online, C_S_LoginReq msg) {
        userService.auth(online, msg.getServerId(), msg.getUserName(), msg.getPassword());
    }
}
