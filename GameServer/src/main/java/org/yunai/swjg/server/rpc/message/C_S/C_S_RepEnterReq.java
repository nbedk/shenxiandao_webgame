package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20401】: 进入副本请求
 */
public class C_S_RepEnterReq extends GameMessage {
    public static final short CODE = 20401;

    /**
     * 副本编号
     */
    private Integer rep;

    public C_S_RepEnterReq() {
    }

    public C_S_RepEnterReq(Integer rep) {
        this.rep = rep;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getRep() {
		return rep;
	}

	public void setRep(Integer rep) {
		this.rep = rep;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_RepEnterReq struct = new C_S_RepEnterReq();
            struct.setRep(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_RepEnterReq struct = (C_S_RepEnterReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getRep());
            return byteArray;
        }
    }
}