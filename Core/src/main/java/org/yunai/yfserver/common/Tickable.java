package org.yunai.yfserver.common;

/**
 * 可tick的
 * User: yunai
 * Date: 13-4-23
 * Time: 下午4:56
 */
public interface Tickable {

    void tick();
}
