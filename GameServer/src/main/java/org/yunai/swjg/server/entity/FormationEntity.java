package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 阵形实体
 * User: yunai
 * Date: 13-5-30
 * Time: 上午2:17
 */
public class FormationEntity implements Entity<Integer> {

    /**
     * 阵形里如果没玩家/伙伴则为0
     */
    public static final int POSITION_EMPTY = -1;

    /**
     * 玩家编号
     */
    private int id;
    /**
     * 位置，每个位置放玩家/伙伴编号，以逗号分隔。<br />
     * 7 4 1 | 10 13 16
     * 8 5 2 | 11 14 17
     * 9 6 3 | 12 15 18
     */
    private String positions;
    /**
     * 可携带伙伴，包括主角本身
     */
    private int max;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPositions() {
        return positions;
    }

    public void setPositions(String positions) {
        this.positions = positions;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}