package org.yunai.swjg.server.module.partner.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.player.VocationEnum;
import org.yunai.swjg.server.module.player.template.ReputationTemplate;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 伙伴模版
 * User: yunai
 * Date: 13-5-29
 * Time: 下午8:38
 */
public class PartnerTemplate {

    /**
     * 伙伴编号
     */
    private short id;
    /**
     * 伙伴名
     */
    private String name;
    /**
     * 职业
     */
    private VocationEnum vocation;
    /**
     * 基础武力
     */
    private int baseStrength;
    /**
     * 基础绝技
     */
    private int baseStunt;
    /**
     * 基础法术
     */
    private int baseMagic;
    /**
     * 基础HP
     */
    private int baseHp;
    /**
     * 所需声望
     */
    private int needReputation;
    /**
     * 所需铜币
     */
    private int needCoin;
    /**
     * 声望等级<br />
     * 该属性约定为通过needReputation来推出
     */
    private ReputationTemplate reputationTemplate;

    public short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public VocationEnum getVocation() {
        return vocation;
    }

    public int getBaseStrength() {
        return baseStrength;
    }

    public int getBaseStunt() {
        return baseStunt;
    }

    public int getBaseMagic() {
        return baseMagic;
    }

    public int getBaseHp() {
        return baseHp;
    }

    public int getNeedReputation() {
        return needReputation;
    }

    public int getNeedCoin() {
        return needCoin;
    }

    public ReputationTemplate getReputationTemplate() {
        return reputationTemplate;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Short, PartnerTemplate> templates;
    /**
     * 根据声望等级做的伙伴模版集合<br />
     * KEY: 声望等级<br />
     * VALUE: 声望等级内的可招募的伙伴模版数组
     */
    private static Map<Short, List<PartnerTemplate>> groupByReputationTemplates;

    public static PartnerTemplate get(short id) {
        return templates.get(id);
    }

    public static List<PartnerTemplate> getByReputationLevel(short reputationLevel) {
        return groupByReputationTemplates.get(reputationLevel);
    }

    public static void load() {
        CsvReader reader = null;
        // 伙伴模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/partner/partner_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                PartnerTemplate template = genPartnerTemplate(reader);
                templates.put(template.id, template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // 拼接groupByReputationTemplates
        int groupByReputationTemplateSize = ReputationTemplate.size() + 1; // +1 是为了，每次获得客栈伙伴模版的时候，会多获得一级伙伴。比如玩家声望等级1，会获得声望等级2内的所有伙伴模版。
        groupByReputationTemplates = new HashMap<>(groupByReputationTemplateSize);
        for (PartnerTemplate template : templates.values()) {
            for (short i = template.reputationTemplate.getLevel(); i <= groupByReputationTemplateSize; i++) {
                List<PartnerTemplate> partnerTemplates = groupByReputationTemplates.get(i);
                if (partnerTemplates == null) {
                    groupByReputationTemplates.put(i, partnerTemplates = new ArrayList<>(1));
                }
                partnerTemplates.add(template);
            }
        }
    }

    private static PartnerTemplate genPartnerTemplate(CsvReader reader) throws IOException {
        PartnerTemplate template = new PartnerTemplate();
        template.id = CsvUtil.getShort(reader, "id", (short) 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.vocation = VocationEnum.valueOf(CsvUtil.getShort(reader, "vocation", (short) 0));
        template.baseStrength = CsvUtil.getInt(reader, "baseStrength", 0);
        template.baseStunt = CsvUtil.getInt(reader, "baseStunt", 0);
        template.baseMagic = CsvUtil.getInt(reader, "baseMagic", 0);
        template.baseHp = CsvUtil.getInt(reader, "baseHp", 0);
        template.needReputation = CsvUtil.getInt(reader, "needReputation", 0);
        template.needCoin = CsvUtil.getInt(reader, "needCoin", 0);
        template.reputationTemplate = ReputationTemplate.getByReputation(template.needReputation);
        return template;
    }
}
